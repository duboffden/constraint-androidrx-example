package com.example_admin_shpp.rxdroid;

import com.example_admin_shpp.rxdroid.models.WeatherData;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

/**
 * Created by Denis Dubovitsky on 20.12.2016.
 * Api for getting weather info
 */

public interface WeatherInterface {
    @GET("weather?")
    Observable<WeatherData> getWeatherData(@Query("q") String cityname, @Query("APPID") String appid);
}
