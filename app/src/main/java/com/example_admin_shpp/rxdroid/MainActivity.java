package com.example_admin_shpp.rxdroid;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.TextView;

import com.example_admin_shpp.rxdroid.models.WeatherData;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainActivity extends AppCompatActivity {
    @BindView(R.id.tv_weather)
    TextView tvWeather;
    @BindView(R.id.iv_weather_icon)
    ImageView ivWeather;
    @BindView(R.id.wv_some_text)
    WebView wvSomeText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getWeather();
        setupWebView();
    }

    private void getWeather() {
        Retrofit retrofit = new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(Constants.BASE_URL)
                .build();

        WeatherInterface weatherService = retrofit.create(WeatherInterface.class);
        Observable<WeatherData> city = weatherService.getWeatherData(Constants.CITY_NAME,
                getString(R.string.openweathermap_api_key));

        city.subscribeOn(Schedulers.newThread())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::setupWeather);
    }

    private void setupWebView() {
        wvSomeText.setBackgroundColor(Color.TRANSPARENT);
        wvSomeText.loadData(getString(R.string.lorem_ipsum), "text/html; charset=utf-8", "utf-8");
    }

    private void setupWeather(WeatherData weatherData) {
        tvWeather.setText(weatherData.getWeather().get(0).getDescription());
        setupIcon(weatherData.getWeather().get(0).getIcon());
    }

    private void setupIcon(String icon) {
        if (icon.equals(Constants.ICON_DAY_CLEAR) || icon.equals(Constants.ICON_NIGHT_CLEAR))
            ivWeather.setImageResource(R.drawable.sunny);
        else
            ivWeather.setImageResource(R.drawable.clouds);
    }
}
