package com.example_admin_shpp.rxdroid;

/**
 * Created by shpp-admin on 21.12.2016.
 */

public class Constants {
    public static final String BASE_URL = "";
    public static final String ICON_DAY_CLEAR = "01d";
    public static final String ICON_NIGHT_CLEAR = "01n";
    public static final String CITY_NAME = "Kropivnitskiy";
}
